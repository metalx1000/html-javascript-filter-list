<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
      #main, body{
        margin:0px;
        font-size:25px;
      }

      body{
        padding:11px;
      }

      @media only screen and (min-width: 600px) {
        body {
          margin-right:200px;
          margin-left:200px;
        }
      }
    </style>
    <script>
      var myitems = ["John","Betty","Sam"];
      //wait for document to load
      document.addEventListener("DOMContentLoaded", function(){
        var main = document.getElementById("main");
        createList("mylist",main,myitems,"echo");
        createList("mylist2",main,myitems,"echo");
      });

      function echo(i){
        var msg = i.innerHTML;
        alert(msg);
      }

    </script>
  </head>
  <body>

    <div id="main" class="container">
      <?php include("list.php");?>
    </div>

  </body>
</html>


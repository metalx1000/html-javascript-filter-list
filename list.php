<style>
  input {
    width:100%;
    font-size:25px;
  }

  ul {
    padding-left:0px;
  }
  li, button {
    box-shadow:inset 0px 1px 3px 0px #91b8b3;
    background:linear-gradient(to bottom, #768d87 5%, #6c7c7c 100%);
    background-color:#768d87;
    border-radius:5px;
    border:1px solid #566963;
    cursor:pointer;
    color:#ffffff;
    font-family:Arial;
    font-size:15px;
    font-weight:bold;
    padding-top:11px;
    padding-bottom:11px;
    text-decoration:none;
    text-shadow:0px -1px 0px #2b665e;
    width:100%;
    margin-bottom:5px;
  }
  li:hover, button:hover {
    background:linear-gradient(to bottom, #6c7c7c 5%, #768d87 100%);
    background-color:#6c7c7c;
  }
  li:active, button:active {
    position:relative;
    top:1px;
  }
</style>
<script>
  var myitems = ["John","Betty","Sam"];

  function filter(input,list){
    var filterVal = input.value.toUpperCase();
    var items = document.getElementsByClassName(list);
    for ( i of  items ){
      if ( i.innerHTML.toUpperCase().indexOf(filterVal) !== -1){
        i.style.display = "";
        }else{
        i.style.display = "none";
      }
    }

  }

  function createList(name,loc,items,func){
    loc.innerHTML += '<input type="text" id="'+name+'_input" onkeyup="filter(this,\''+name+'_item\')" placeholder="">';
    loc.innerHTML += '<ul id="'+name+'_list"></ul>';
    for ( i of items ){
      var list = document.getElementById(name+"_list");
      list.innerHTML += "<li onclick='"+func+"(this)' class='"+name+"_item'>"+i+"</li>";
    }
  }
</script>
